# Asteria-old
 Team 3 game project:
 
Michael Mederos Task:
    1. Create Boss scene
    2. Finish the Level 1 scene.
    3. Assist with enemy AI
    4. Off Mesh Link creation.
    5.Audio implementation.
    
 Vin Martinez Task:
       1. Player Controller/Agents' Animation Controller
       2. Boss Enemy AI
       3. Main Menu Screen
       4. Art Assets
       5. Player Health and Boss Enemy Health
       
Corey Knighten Task:
       1.General AI
       2.Enemy AI
       3.Enemy Animator Controllers
       4.Assist in basic game mechanics
       5.Enemy control script
       